'use strict';

let path = require('path');
let gulp = require('gulp');
var Stream = require('stream');

let preprocess   = require('gulp-preprocess');
let rename       = require('gulp-rename');
let typescript   = require('gulp-typescript');
let sourcemaps   = require('gulp-sourcemaps');
let uglify       = require('gulp-uglify');

module.exports = function (projectDir, callable) {
    let src = [
        'node_modules/angular2/typings/browser.d.ts',
        projectDir + '/src/typescript/**/*.ts'
    ];

    /**
     * Do nothing streaming
     *
     * @return {Stream}
     */
    function noop() {
        let stream = new Stream.Transform({ objectMode: true });

        stream._transform = function (file, unused, callback) {
            callback(null, file);
        };

        return stream;
    }

    let cwd = path.relative(projectDir, gulp.args.cwd).replace(/\\/g, '/');

    return function () {
        gulp.src(src)
            .pipe(gulp.args.build ? noop() : sourcemaps.init())
            .pipe(typescript(typescript.createProject('tsconfig.json')))
            .pipe(uglify())
            // .pipe(rename({ suffix: '.min' }))
            .pipe(gulp.args.build ? noop() : sourcemaps.write())
            .pipe(gulp.dest(projectDir + '/public/javascripts'))
        ;
    };
};
