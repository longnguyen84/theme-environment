'use strict';

let gulp = require('gulp');

module.exports = function (projectDir, callable) {
    return function () {
        gulp.watch([
            projectDir + '/src/typescript/**/*.ts',
            projectDir + '/src/scss/**/*.scss',
            '!' + projectDir + '/src/scss/bootstrap/*',
            '!' + projectDir + '/src/scss/elements/_sprites.scss'
        ], callable);
    };
};
