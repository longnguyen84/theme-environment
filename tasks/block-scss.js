'use strict';

const path = require('path');

const autoprefixer = require('gulp-autoprefixer');
const cleanCSS     = require('gulp-clean-css');
const gulp         = require('gulp');
const preprocess   = require('gulp-preprocess');
const rename       = require('gulp-rename');
const sass         = require('gulp-sass');
const sourcemaps   = require('gulp-sourcemaps');

const helper = require('../helper');


module.exports = function (projectDir, callable) {
    let results = helper.scanWorkingDirs(projectDir, /src\\scss/);

    if (gulp.args.filter) {
        results = helper.filter(results, gulp.args.filter.split(','));
    }

    let cwd = process.cwd().replace(/\\/g, '/');

    return function () {
        results.map(src => {
            gulp.src(path.join(src, '*.scss'))
                .pipe(preprocess({context: {
                    cwd: cwd,
                    node_modules: cwd + '/node_modules'
                }}))
                .pipe(gulp.args.build ? helper.noopStream() : sourcemaps.init())
                .pipe(sass())
                .pipe(autoprefixer('last 10 version'))
                .pipe(cleanCSS({
                    level: 0,
                    skipProperties: ['cx', 'cy', 'rx', 'ry'],
                    specialComments: 0,
                    roundingPrecision: -1
                }))
                .pipe(rename({ suffix: '.min' }))
                .pipe(gulp.args.build ? helper.noopStream() : sourcemaps.write('.', {includeContent: false}))
                .pipe(gulp.dest(path.join(src, '../../dist/css')))
            ;
        });
    }
};
