const babel = require('gulp-babel');
const concat = require('gulp-concat');
const fs   = require('fs');
const gulp = require('gulp');
const path = require('path');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');
const webpack = require('gulp-webpack');

const helper = require('../helper');

module.exports = function (projectDir, callable) {
    let results = helper.scanWorkingDirs(projectDir, /src\\js/);

    if (gulp.args.filter) {
        results = helper.filter(results, gulp.args.filter.split(','))
    }

    return function () {
        results.map(src => {
            gulp.src(path.join(src, '*.js'))
                .pipe(gulp.args.build ? helper.noopStream() : sourcemaps.init())
                .pipe(babel({
                    comments: false,
                    minified: true,
                    plugins: [
                        [helper.require('babel-plugin-transform-runtime'), {
                            helpers: false,
                            polyfill: false,
                            regenerator: true,
                            moduleName: 'babel-runtime'
                        }],
                        [helper.require('babel-plugin-external-helpers')],
                    ],
                    presets: [helper.require('babel-preset-es2015')],
                }))
                .pipe(concat('main.js'))
                .pipe(rename({suffix: '.min'}))
                .pipe(gulp.args.build ? helper.noopStream() : sourcemaps.write('.', {includeContent: false}))
                .pipe(gulp.dest(path.join(src, '../../dist/js')))
            ;
        });
    }
};
