'use strict';

let fs   = require('fs');
let path = require('path');
let gulp = require('gulp');
var Stream = require('stream');

let autoprefixer = require('gulp-autoprefixer');
let cleanCSS     = require('gulp-clean-css');
let preprocess   = require('gulp-preprocess');
let rename       = require('gulp-rename');
let sass         = require('gulp-sass');
let sourcemaps   = require('gulp-sourcemaps');

module.exports = function (projectDir, callable) {
    let files = fs.readdirSync(projectDir + '/src/scss');
    let src = [];

    files.forEach(function (file) {
        if (file.match(/^[a-z]{1}(.*?)\.scss$/)) {
            src.push(projectDir + '/src/scss/' + file);
        }
    }, src);

    /**
     * Do nothing streaming
     *
     * @return {Stream}
     */
    function noop() {
        let stream = new Stream.Transform({ objectMode: true });

        stream._transform = function (file, unused, callback) {
            callback(null, file);
        };

        return stream;
    }

    let cwd = path.relative(projectDir, gulp.args.cwd).replace(/\\/g, '/');

    return function () {
        gulp.src(src)
            .pipe(preprocess({ context: {
                cwd: cwd,
                node_modules: cwd + '/node_modules'
            } }))
            .pipe(gulp.args.build ? noop() : sourcemaps.init())
            .pipe(sass().on('error', sass.logError))
            .pipe(autoprefixer('last 10 version'))
            .pipe(cleanCSS({
                keepSpecialComments: 0,
                roundingPrecision: -1
            }))
            .pipe(rename({ suffix: '.min' }))
            .pipe(gulp.args.build ? noop() : sourcemaps.write())
            .pipe(gulp.dest(projectDir + '/public/stylesheets'))
        ;
    };
};
