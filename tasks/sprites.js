'use strict';

let gulp = require('gulp');
let spritesmith = require('gulp.spritesmith');

module.exports = function (projectDir, callable) {
    return function () {
        let spriteData = gulp
            .src(projectDir + '/public/images/sprites/*.png')
            .pipe(spritesmith(require(__dirname + '/../sprites.configs.js')(projectDir)))
        ;

        spriteData.img.pipe(gulp.dest('.'));
        spriteData.css.pipe(gulp.dest('.'));

        return spriteData;
    };
};
