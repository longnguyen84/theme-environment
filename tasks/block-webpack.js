const fs   = require('fs');
const gulp = require('gulp');
const path = require('path');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const _ = require('lodash');

const helper = require('../helper');

module.exports = function (projectDir, options = {}) {
    let results = helper.scanWorkingDirs(projectDir, /src[/\\]js/);
    const webpackConfig = require(process.cwd() + '/webpack.config.js');

    if (gulp.args.filter) {
        results = helper.filter(results, gulp.args.filter.split(','))
    }

    return function () {
        results.map(src => {
            const input = path.resolve(src, 'main.js');
            const output = path.resolve(src, '../../dist/js');
            const configs = _.cloneDeep(webpackConfig);

            configs.output.library = helper.classify(src.match(/[/\\]([\w\d-]*)[/\\]src[/\\]js/i)[1]);

            gulp.src(input)
                .pipe(webpackStream(configs, webpack))
                .pipe(gulp.dest(output))
            ;
        });
    }
};
