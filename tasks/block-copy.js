const fs   = require('fs');
const path = require('path');
const gulp = require('gulp');
const copy = require('gulp-copy');
const helper = require('../helper');

module.exports = function (projectDir, callable) {
    let results = helper.scanWorkingDirs(projectDir, /src\\js/);

    if (gulp.args.filter) {
        results = helper.filter(results, gulp.args.filter.split(','))
    }

    return function () {
        results.map(src => {
            gulp.src(path.join(src, '*.js'))

            ;
        });
    }
};
