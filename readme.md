# Modern Front-End Development

The life of a front-end developer has never been easier.

## Requirement

* [Git][3]
* [NodeJS][1]
* [Ruby][2]

## NPM

NPM is the default package manager for the javascript runtime environment Node.js

## GEM

RubyGems is a package manager for the Ruby programming language

## Git

The version control system of choice for most developers nowadays. It makes sharing code easy and frictionless.

## Gulp

Gulp is a toolkit that helps you automate painful or time-consuming tasks in our development workflow.

## SCSS

Sass is the most mature, stable, and powerful professional grade CSS extension language in the world.

## SCSS Linter

`scss-lint` is a tool to help keep our SCSS files clean and readable.

## JSHint

Helps to detect errors and potential problems in code.

## JSCS

JSCS is a code style linter/formatter for programmatically enforcing our style guide.

[1]: https://nodejs.org/en/
[2]: https://www.ruby-lang.org/en/
[3]: https://git-scm.com/
