const gulp = require('gulp');
const path = require('path');
const webpack = require('webpack');

const helper = require('./helper');

module.exports = {
    output: {
        filename: '[name].min.js',
        sourceMapFilename: '[name].min.js.map'
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /(node_modules|jspm_packages)/,
            use: {
                loader: 'babel-loader',
                options: {
                    plugins: [
                        [helper.require('babel-plugin-external-helpers')],
                        [helper.require('babel-plugin-transform-runtime'), {
                            helpers: false,
                            polyfill: false
                        }]
                    ],
                    presets: [helper.require('babel-preset-es2015')],
                }
            }
        }]
    },
    resolve: {
        modules: [
            path.join(__dirname, 'node_modules'),
            path.join(__dirname, 'jspm_packages/github'),
            path.join(__dirname, 'jspm_packages/npm')
        ],
        alias: {
            'component': path.resolve(gulp.args.projectDir, 'component'),
            'widget': path.resolve(gulp.args.projectDir, 'widget'),
            'widget_type': path.resolve(gulp.args.projectDir, 'widget_type')
        },
        extensions: ['.js', '.ts']
    },
    externals: [{
        lodash: '_',
        jquery: 'jQuery',
        'hammerjs@2.0.8': 'Hammer'
    }, function (context, request, callback) {
        if (/\.\.\/types\//.test(request) || /^(npm:|github:)/.test(request)) {
            return callback(null, 'null');
        }

        let blockDirectories = request.match(/^(component|develop|widget|widget_type)\/([\w\d-]*)\/.*/i);

        if (blockDirectories) {
            return callback(null, helper.classify(blockDirectories[2]));
        }

        callback();
    }],
    plugins: [
        new webpack.SourceMapDevToolPlugin({
            filename: '[name].min.js.map',
            noSources: true
        }),
        new webpack.BannerPlugin('Author: Nilead Team <support@nilead.com>\nHomepage: https://nilead.com'),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true
        }),
    ]
};
