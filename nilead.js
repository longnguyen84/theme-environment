/**
 * Created by Rubikin Team.
 * ========================
 *
 * This file is a part of Nilead project.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

'use strict';

// jscs:disable
// jshint ignore: start

let fs = require('fs');
let path = require('path');
let gulp = require('gulp');
let exec = require('child_process').exec;
let minimist = require('minimist');

let helper = require('./helper');

module.exports = Nilead;

function Nilead (gulpfile) {
    this.gulpfile = gulpfile;
    this.timer = null;
}

Nilead.prototype.run = function() {
    /**
     * @type {Object}
     */
    let argv = minimist(process.argv.slice(2));
    let processor = exec('gulp ' + helper.toArgv({
        cwd: __dirname,
        gulpfile: this.gulpfile,
        filter: argv._.join(','),
        build: argv.build
    }));

    processor.stdout.on('data', this.log.bind(this));
    processor.stderr.on('data', this.error.bind(this));
};

Nilead.prototype.log = function(message) {
    clearTimeout(this.timer);
    this.timer = setTimeout(console.log, 3000, '');

    if (message instanceof Buffer) {
        message = message.toString();
    }

    message = message.replace(/[\r\n]/g, '').trim();
    let segments = message.match(/^(.*?)\[\d{2}:\d{2}:\d{2}](.*?)$/);

    if (!message || 0 === message.search('Using gulpfile') || 0 === message.search('Working') || 0 === message.search('Version: webpack')) {
        return;
    }

    if (segments) {
        this.log(segments[1]);
        this.log(segments[2]);

        return;
    }

    let time = new Date().toISOString().substr(11, 12);

    message = message
        .replace(/^Starting/, '- Starting')
        .replace(/^Finished/, '+ Finished')
    ;

    console.log('[' + time + ']', message);
};

Nilead.prototype.error = function(message) {
    if (message instanceof Buffer) {
        message = message.toString();
    }

    console.log('');
    console.log('--------------------------------------------------------------------------------');
    console.error(message.trim());
    console.log('--------------------------------------------------------------------------------');
    console.log('');
};
